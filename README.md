# Spark: analyse et consolidation des données d'observation astronomiques

Dans ce TP, on va pousser plus loin l'utilisation de Spark et travailler sur une volumétrie de données plus importante.

> Ce TP manipulant éventuellement de grosses données il faut:
>
> - Supprimer les répertoires HDFS inutiles: `hdfs dfs -rm -r repertoire-a-supprimer`
> - Lancer ensuite `hdfs dfs -expunge` pour vider régulièrement la poubelle HDFS (ignorer les erreurs de permissions)

L'utilitaire [`screen`](https://en.wikipedia.org/wiki/GNU_Screen) permet (entre autres) de conserver un shell actif tout en se déconnectant ([quickref](http://aperiodic.net/screen/quick_reference)).
Il ne faut pas hésiter à l'utiliser quand on lance des jobs longs sur le cluster.

Pour démarrer ce TP, il est possible de reprendre votre code du TP précédent ou celui du corrigé: https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-spark-2018-correction

## Rendu

Ce TP est à rendre pour le **17/11/2019**.
Il se fera en binôme.
Il est demandé de déposer un fichier `.zip` sur tomuss dans la case `TP_SPARK2`.
Cette archive comprendra:

- Le code source
- Le fichier `etudiants.yml` complété
- Le fichier `COMPTE_RENDU.md` complété avec en particulier:
  - Les étudiants du binôme (repris depuis le fichier `etudiants.yml`)
  - les problèmes qui se sont posés et les solutions apportées
  - les extraits pertinents du code source

Barême indicatif (sur 20):

- statistiques sur les attributs: 5 points
- partitionnement des données selon un attribut: 5 points
- partitionnement des données selon un nombre quelconque d'attributs: 5 points
- évaluation de la qualité des configurations de partitionnement: 5 points
- bonus recouvrements: 4 points
- bonus intervalles de taille variable: 4 points
- bonus recherche de sources similaires: 4 points

## Problème posé

On considère le jeu de données du TP précédent.
On rappelle que l'on dispose de deux relations `Source` et `Object`.
`Source` décrit les observations et `Object` les objets astrophysiques.
La plupart des `Source` référencent un `Object`.
On souhaite pouvoir rapprocher de nouvelles observations (_i.e._ de nouvelles données dans `Source`) d'observations existantes, c'est-à-dire savoir s'il existe si on peut leur attribuer un `objectId`.

Pour cela on va tout d'abord calculer pour chaque attribut de la relation `Source` son écart type moyen par objet.
On normalisera ensuite cet écart type vis-à-vis de la taille de l'intervalle de valeurs que peut prendre cet attribut.

Après avoir trié les attributs selon cet écart-type normalisé, on effectuera une répartition des données en blocs de taille inférieure à 128Mo (qui est la taille d'un bloc de données HDFS) en s'appuyant sur la valeurs des _n_ attributs qui varient le moins pour un même dans un objet.
On commencera avec une valeur _n=1_, puis on fera des essais avec des valeurs plus grandes.

Pour chaque configuration de partionnement utilisée on mesurera son efficacité en quantifiant le nombre d'objets dont les sources se retrouvent à cheval sur plusieurs blocs.

Il est possible de proposer, en l'argumentant, d'autres critères de sélection des attributs de partitionnement des données.

## Détails

Les fichiers complets se trouvent dans le répertoire `hdfs:///petasky/Source`. 
Il est recommandé de travailler tout d'abord sur le fichier `source-sample`, avant de tester ensuite avec le fichier `hdfs:///petasky/Source/Source-001.csv` puis d'exécuter le job complet.

### Calcul des statistiques sur les attributs

Certains attributs sont à exclure:

- les attributs `xxxxId`
- les attributs mentionnant les objects

On pourra s'appuyer sur l'objet `PetaSkySchema` (fichier `src/main/scala/schema/PetaSkySchema.scala`) ou sur le module `petasky_schema` (fichier `python/petasky_schema.py`) fournis dans le [corrigé du TP précédent](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-spark-2018-correction) pour récupérer facilement le tableau des attributs (et donc leur position).

On réfléchira bien à la manière de regrouper les lignes aux différentes étapes du calcul.
Deux approches sont possibles: garder le vecteur complet des attributs ou commencer par éclater chaque ligne en un triplet (`objectId`, attribut, valeur).

Si besoin, et au vu du faible nombre d'attributs, le tri final pourra se faire hors Spark.

### Partitionnement des données

Pour partionner les données, il faut déterminer des blocs de valeurs pour les attributs de partionnement, puis répartir les lignes de `Source` selon le bloc correspondant à la valeur des attributs de partionnement.

Si on prend un seul attribut _A_, il faut créer _k_ intervalles couvrant l'ensemble des valeurs, i.e. un partionnement de l'intervalle [_vMin_, _vMax_] où _vMin_ (resp. _vMax_) est la valeur minimale (resp. maximale) de _A_ dans `Source`.

Une stratégie simple consiste à estimer combien de blocs de 128Mo il faut pour stocker `Source` et à prendre cette valeur pour _k_, puis à considérer que l'on va diviser [_vMin_, _vMax_] en des intervalles de taille égale.

Cette stratégie peut se généraliser facilement à une division d'un rectangle, d'un cube ou d'un hypercube selon le nombre d'attributs.

**Remarque:** la plupart des opérations nécessitent juste d'attribuer un identifiant d'intervalle à chaque ligne de `Source`. Il ne faut pas construire explicitement la liste des sources d'un intervalle, car cette liste risque de prendre trop de place.

L'écriture des données partionnées dans HDFS se fera selon le principe suivant:

- on associe à chaque élément de la partition (_i.e._ chaque petit intervalle, rectangle, etc) un nom de fichier HDFS qui dépend de son identifiant
- on écrit chaque ligne de `Source` dans le bon fichier:
  - en Scala, afin de simplifier l'écriture d'un RDD dans des fichiers en fonction d'une valeur, on pourra utiliser la fonction `TIW6RDDUtils.writePairRDDToHadoopUsingKeyAsFileName` (fournie dans le fichier `TIW6RDDUtils.scala`).
  - en Python, il faudra itérer pour chaque intervalle le travail suivant: filtrer le RDD pour ne garder que les lignes `Source` concernant l'intervalle, puis écrire ces lignes dans le bon fichier.

### Qualité du partionnement

Pour estimer la qualité de ce partionnement:

- calculer le nombre d'objets qui sont répartis sur plusieurs partitions;
- calculer le nombre de partition qui contiennent trop de lignes, c'est-à-dire dont on estime que leur représentation sur disque dépassera 128Mo.

Ces calculs peuvent se faire sans que les données aient été réécrites concrètement sur HDFS (il suffit en effet de savoir dans quel fichier chaque ligne sera écrite).

Essayer plusieurs configurations de partionnement et indiquer celle qui vous semble la plus pertinente.

### Bonus: recouvrements

Si les valeur d'un attribut est proche de la frontière d'une partition (_e.g._ d'intervalle), les sources similaires peuvent se retrouver dans une partition voisine.

Un autre moyen de procéder pour prendre en compte ce phénomène est d'autoriser les chevauchements de partitions en dupliquant les source qui sont proches de la frontière dans les partitions voisines.

Il faudra ensuite modifier le calcul de qualité du partitionnement: un objet est réparti sur plusieurs partition seulement si il n'existe pas de partition qui contient toutes ses sources

### Bonus: intervalles de taille variable

La distribution des données n'est pas uniforme.
Un moyen d'améliorer le partitionnement consiste à utiliser des intervalles de taille variable (plus petits dans le zones denses).
Pour cela, on peut par exemple redécouper les partitions ayant trop de sources.
Pour partitionner, il faudra alors construire et utiliser une structure permettant de retrouver la bonne partition en fonction de la valeur des attributs.

### Bonus: recherche de sources similaires

Écrire un programme qui effectue la recherche des _m_ sources (distance euclidienne sur les attributs) les plus similaires à une source donnée en les cherchant dans la bonne partition.
